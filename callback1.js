module.exports.getBoardInfo = (boardID, boardData, callback) => {
  setTimeout((err, data) => {                                             //setTimeOut to delay the execution by 2 seconds
    if (
      typeof boardData == "object" &&                                    // Data Validation for the required type of data for the program to execute
      typeof boardID == "string" &&
      boardData !== null
    ) {
      data = boardData.find(function (board) {                          // data stores the random data associated with the particular boardId
        return board.id === boardID;
      });
      if (!data) {
        err = new Error("Data not found");                              // Error message to be displayed if the data that is looked upon is not found
      }
    }
    if (err) {
      callback(err);
    } else {
      callback(null, data);
    }
  }, 2 * 1000);
};
