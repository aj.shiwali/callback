const { getBoardInfo } = require("./callback1");
const { getBoardList } = require("./callback2");
const { getCardData } = require("./callback3");

module.exports.cardForMindList = (
  boardName,
  listName,
  boardData,
  listData,
  cardsData
) => {
  setTimeout(() => {
    for (let i = 0; i < boardData.length; i++) {
      if (boardData[i].name === boardName) {
        getBoardInfo(boardData[i].id, boardData, (err, data) => {
          getBoardList(boardData[i].id, listData, (err, data) => {
            if (data) {
              const lists = data.filter((list) => listName.includes(list.name));
              if (!lists) {
                console.log("list name not found");
              } else {
                lists.forEach((list) => {
                  getCardData(list.id, cardsData, (err, data) => {
                    if (!err) {
                      console.log(data);
                    } else {
                      console.log(err);
                    }
                  });
                });
              }
            } else {
              console.log(err);
            }
          });
        });
      }
    }
  }, 2 * 1000);
};
