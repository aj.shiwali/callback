module.exports.getCardData = (cardID, cardData, callback) => {
  setTimeout((err, data) => {
    if (
      typeof cardData == "object" &&
      typeof cardID == "string" &&
      cardData !== null
    ) {
      data = cardData[cardID];
      if (!data) {
        err = new Error("Data not found");
      }
    }
    if (err) {
      callback(err);
    } else {
      callback(null, data);
    }
  }, 2 * 1000);
};
