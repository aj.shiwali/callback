module.exports.getBoardList = (boardID, listData, callback) => {
  setTimeout((err, data) => {
    if (
      typeof listData == "object" &&
      typeof boardID == "string" &&
      listData !== null
    ) {
      data = listData[boardID];
      if (!data) {
        err = new Error("Data not found");
      }
    }
    if (err) {
      callback(err);
    } else {
      callback(null, data);
    }
  }, 2 * 1000);
};
