const path = require("path");
const listData = require(path.join(__dirname, "../data/lists.json"));
const cardData = require(path.join(__dirname, "../data/cards.json"));
let keys = Object.keys(listData);
const { getCardData } = require("../callback3");

let keyData = listData["mcu453ed"];
let keyId = keyData[Math.floor(Math.random() * keyData.length)].id;

const callBack = (err, data) => {
  if (err) {
    console.log(err.message);
  } else {
    console.log(data);
  }
};
getCardData(keyId, cardData, callBack);
