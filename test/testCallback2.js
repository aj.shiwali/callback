const path = require("path");
const listData = require(path.join(__dirname, "../data/lists.json"));
let keys = Object.keys(listData);
const { getBoardList } = require("../callback2");

let boardId = keys[Math.floor(Math.random() * keys.length)];

const callBack = (err, data) => {
  if (err) {
    console.log(err.message);
  } else {
    console.log(data);
  }
};

getBoardList(boardId, listData, callBack);
