const path = require("path");
const boardData = require(path.join(__dirname, "../data/boards.json"));
const listData = require(path.join(__dirname, "../data/lists.json"));
const cardData = require(path.join(__dirname, "../data/cards.json"));
const { cardForMindList } = require("../callback5");

cardForMindList("Thanos", ["Mind", "Space"], boardData, listData, cardData);
