const path = require("path");
const boardData = require(path.join(__dirname, "../data/boards.json"));
const { getBoardInfo } = require("../callback1");

let boardId = boardData[Math.floor(Math.random() * boardData.length)].id;

const callBack = (err, data) => {                                       // Callback function to be executed to log the data to the console
    if (err) {
        console.log(err.message);
    }
    else {
        console.log(data);
    }
}
getBoardInfo(boardId, boardData, callBack);                             // passing control back to the function that invoked the callback function

