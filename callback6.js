const { getBoardInfo } = require("./callback1");
const { getBoardList } = require("./callback2");
const { getCardData } = require("./callback3");

module.exports.getAllCardsFromThanosBoard = (boardData, cardData, listData) => {
  setTimeout(() => {
    if (
      typeof boardData !== "object" ||
      typeof listData !== "object" ||
      typeof cardData !== "object"
    ) {
      let err = new Error("Data is Invalid");
      console.log(err);
    } else {
      boardData.filter((boards) => {
        if (boards.name === "Thanos") {
          let boardArray = [];
          boardArray.push(boards);
          getBoardInfo(boards.id, boardArray, (err, data) => {
            if (err) {
              console.log(err);
            } else {
              let dataArray = [];
              dataArray.push(data);
              dataArray.filter((dataId) => {
                getBoardList(dataId.id, listData, (err, data) => {
                  if (err) {
                    console.log(err);
                  } else {
                    data.forEach((outputData) => {
                      getCardData(outputData.id, cardData, (err, data) => {
                        if (err) {
                          console.log(err);
                        } else {
                          data.forEach((outputData) => {
                            console.log(outputData);
                          });
                        }
                      });
                    });
                  }
                });
              });
            }
          });
        }
      });
    }
  }, 2 * 1000);
};
